`<MARKDOWN>`
==========

This is a proposal to include the `<markdown>` tag support in browsers.


## Problem description

Markdown is the currently accepted method to author content. Yet, presentation is still
rendered from HTML and the markdown code is a distant and inaccessible secret source.

This creates a problem that didn't exist when content was authored in HTML. Users could
consume/copy/inspect the original source easily. Every browser have a view-source functionality.
HTML was what the user was versed while authoring content, and serving it as-is allowed for a rich
set of development tools to evolve. Browsers are close to
Integrated Development Environment (IDE) for HTML and Javascript and other first-class components.

But with markdown content, after it is published, the source is now html and the relation is broken.
This proposal brings back that freedom to modern users. The rendering will happen from actual
markdown code delivered to the client.

## Supported features

TODO.

### code

TODO: Advanced implementations contain support for code syntax highlight by adding a language name
next to the three backticks code block. Should this be in scope?

### math and diagrams

Maybe featurecreeping, but having math and markdown is a often requested feature on most cms. And mathML is not a syntax anyone wants to work with.

Likewise, UML and other diagrams from mermaidjs

TODO: is there a consensus on how to render math on browser? good alternative can be mathjax with svg render. Latex et all have different syntax see [comparisson](https://squidfunk.github.io/mkdocs-material/reference/math/)

Should mathML be the output for rendering?

### icons

Icon syntax such as `:smile:` are not necessary. The browser already have more than enough methods to
select symbols and images.

### Proper escaping

On most implementations, escaping is hell. It is impossible to publish a markdown document on
markdown without employing html to output a code block with the three backticks glyphs. Same for
escaping super/underscript hints, etc.

On the browser this should work consistently and not have untested edge-cases.

### Layout and grids

Most layout should be left to html. Specially more complex ones.

### tables

tables is another feature where implementations don't agree much on syntax.
Maybe this will require a syntax attribute?

## backward compatibility

A polyfill must be provided as the reference implementation.

### future proof

The means used to provide the backward compatibility javascript code might not impact
performance when the support for the new tag is available.

1. Option A. Simply provide a script inside the tag. The script will detect if instantiated from
   within a tag and already apply styling. Further support for the tag will understand there's no
   script tags inside markdown and it will be ignored. Not a security feature.
```html
<markdown>
<script src="/markdown-support.js"></script>
# this will render as markdown
</markdown>
```

Option selected here will impact [Versioning](#Versioning) bellow.

### Versioning

Likewise with [backward compatibility](#future proof) hacks to provide polyfills,
there should be a need to provide polyfills of planned fetures/versions.

The existing method to provide polyfills should be used in this case, but now
with support for the delivery backed into the tag as part of the design for version 1.

```html
<markdown>
<script markdownversion="1.2" src="/markdown1.2-support.js"></script>
# This uses features from markdown tag 1.2
</markdown>
```

If the host platform supports version 1.2, the script will be ignored.

TODO: This can be abused to deliver plugins/themes/always on script? `<markdown><script markdownversion="999" ...`? Is this desirable?

## Security

All the usual considerations of html should be the same for the new proposed tag.

This is not an attempt to show user generate code in a sanitized way. Although consideration can be taken for this direction.

### Attributes

#### inherit from A

Links created from markdown should inherit values from the tag's A inherited attributes, such as `rel`, `target`, etc. TODO: provide comprehensive list.

e.g. `<markdown rel="noreferrer nofollow">`

